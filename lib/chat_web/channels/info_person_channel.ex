defmodule ChatWeb.InfoPersonChannel do
  use ChatWeb, :channel

  @impl true
  def join("info_person:lobby", _payload, socket) do
    IO.puts("------------------------------------------------------------------------------")
    IO.inspect(socket)
    # if authorized?(payload) do
    #   {:ok, socket}
    # else
    #   {:error, %{reason: "unauthorized"}}
    # end

    {:ok, socket}
  end

  def handle_in("info_person:menssage", payload, socket) do
    IO.puts "entrando no handle_in de message"

    {:ok, socket}
  end

  # Channels can be used in a request/response fashion
  # by sending replies to requests from the client
  @impl true
  def handle_in("ping", payload, socket) do
    {:reply, {:ok, payload}, socket}
  end

  # It is also common to receive messages from the client and
  # broadcast to everyone in the current topic (info_person:lobby).
  @impl true
  def handle_in("shout", payload, socket) do
    IO.puts "aaaaaaaaaaa"
    IO.inspect(payload)

    broadcast socket, "shout", payload
    {:noreply, socket}
  end

  # Add authorization logic here as required.
  defp authorized?(_payload) do
    true
  end

end
